# Experiments with creating UI Application for Linux

## Electron

Electron is Node JS library used for creating crossplatform UI applications using HTML, CSS and JavaScript.
Later on this packages can be converted to executables for ech platform using electron-packager.

## PyGTK

PyGTK is a library for python. Newer version of PyGTK is called PyGTK Object.

## PyGTK - code only

Creates UI application only via code.

## PyGTK - Glade

Glade is graphical application for creating UI components designed to be used with PyGTK.
